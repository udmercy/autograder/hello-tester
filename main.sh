#!/bin/bash

./build.sh 1> ./build.out 2> ./build.err
status=$?
echo "BUILD"
echo "-----"
echo ""
if test $status -eq 0
then
	echo "Passed"
	echo ""
else
	echo "Failed"
	echo ""
	echo "Build Output:"
	echo ""
	echo "```"
	cat ./build.out
	echo "```"
	echo ""
	echo "Build Error:"
	echo ""
	echo "```"
	cat ./build.err
	echo "```"
	echo ""
	exit 0
fi

echo "TESTS"
echo "-----"
echo ""
for fin in ./tests/*.in; do
	answer="${fin%.*}.out"
	filename="${fin##*/}"
	testname="${filename%.*}"
	fout="./outputs/${answer##*/}"

	sudo su student -c "cat $fin | bash run.sh > $fout"
	diff $answer $fout 2>/dev/null > /dev/null
	status=$?
	if test $status -eq 0
	then
		echo "✅ $testname"
		echo "" 
	else
		echo "❌ $testname"
		if [ -z ${var+x} ]; then
			echo "expected"
			echo ""
			echo "```"
			cat $answer
			echo "```"
			echo ""
			echo "output"
			echo ""
			echo "```"
			cat $fout
			echo "```"
			echo ""
			echo "difference"
			echo ""
			echo "```"
			diff -y $answer $fout 
			echo "```"
		fi
	fi
done
