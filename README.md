# Hello World Tester

This is an example tester for a hello world program.

# Use

First install.

`./install.sh`

Then run it. This is done by mounting the submission folder as a bind mount, and optionally including the environment variable VERBOSE to get more detailed output

`docker run -e VERBOSE -v "$(pwd)"/solution:/home/tester/submission hello-tester`

# Modification

To modify this for any assignment, do the following:

1. Modify build.sh such that it will build the submission.
2. Modify run.sh such that it will run the built submission.
3. Create files in the tests folder: inputs and their corresponding outputs should have the same file names, modulo the .in and .out file extensions.

