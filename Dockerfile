FROM ubuntu:focal

# Create users
RUN adduser --disabled-password --gecos '' tester
RUN adduser --disabled-password --gecos '' student

# Install necessary tools
RUN apt-get update 
RUN apt-get install -y python3
RUN apt-get install -y sudo

# Passwordless sudo for our tester
RUN usermod -aG sudo tester
RUN echo "tester ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
RUN echo "Set disable_coredump false" >> /etc/sudo.conf
RUN touch ./.sudo_as_admin_successful

#TODO Mount submission
# Set up our home directory with the submission and tests
WORKDIR /home/tester
COPY tests ./tests
COPY run.sh ./run.sh
COPY main.sh ./main.sh
COPY build.sh ./build.sh
#COPY submission ./submission   
RUN mkdir outputs 
RUN chown -R tester:tester ./tests
#RUN chown -R tester:tester ./submission
RUN chown -R tester:tester ./outputs
RUN chown tester:tester ./*.sh
RUN chmod -R 444 tests/*.in
RUN chmod -R 400 tests/*.out
RUN chmod 777 outputs
RUN chmod +x run.sh

# Run our main script as tester
USER tester
CMD ./main.sh
